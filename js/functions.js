const sideMenuNav = document.getElementById('side-nav');
const backgroundSideNav = document.getElementById('side-nav-background');
var toggleStatus = 1;

function showMenuSide(){
    if(toggleStatus == 1){
        sideMenuNav.style.left = 0;
        backgroundSideNav.style.left = 0;
        toggleStatus = 0;
    }
    else if(toggleStatus == 0){
        sideMenuNav.style.left = '-110%';
        backgroundSideNav.style.left = '-110%';
        toggleStatus = 1;
    }
}


