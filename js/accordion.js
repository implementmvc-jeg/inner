$(function () {
    $(".qna-card-header").click(function () {
        let $this = $(this);
            $qnaCardBody = $this.next(".qna-card-body");
        if($qnaCardBody.hasClass("active")){
            $qnaCardBody.removeClass("active").slideUp()
            $this.children("span").removeClass("fa-angle-up").addClass("fa-angle-right")
        } else {
            $(".qna-card .qna-card-body").removeClass("active").slideUp()
            $(".qna-card .qna-card-header span").removeClass("fa-angle-up").addClass("fa-angle-right")
            $qnaCardBody.addClass("active").slideDown()
            $this.children("span").removeClass("fa-angle-right").addClass("fa-angle-up").style().color("blue")
        }
    })
});